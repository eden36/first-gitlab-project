import os
import atexit
from pathlib import Path
from platform import node
from flask import Flask
from dotenv import load_dotenv
from app import routes
from app.statestore import StateStore

APP_NAME = 'flask-boilerplate'


def load_config():
    env_path = Path('.') / '.env'
    if 'FLASK_ENV' in os.environ:
        env_path = '{path}.{env}'.format(
            path=env_path,
            env=os.getenv('FLASK_ENV')
        )
    load_dotenv(
        dotenv_path=env_path
    )


def create_app():
    """
    Create flask app object instance
    """
    app = Flask(__name__)
    register_blueprints(app)

    app.config['STORE_STATE'] = False
    if 'STORE_STATE' in os.environ:
        app.config['STORE_STATE'] = True
        ss = StateStore(
            hostname=os.getenv('REDIS_HOST'),
            node=node()
        )
        ss.init_state()
        atexit.register(ss.stop_state)

    return app


def register_blueprints(app):
    """
    Register Flask blueprints necessary in production deployment.
    """
    app.register_blueprint(routes.api.health_bp)
    app.register_blueprint(routes.api.client_ip_bp)
    return None

